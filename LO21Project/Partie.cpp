#include "Partie.h"
#include <iostream>
#include <iomanip>
#include <string>

namespace SchottenTotten {
	//// Singleton:
	//Partie* Partie::instanceUnique = nullptr;
	//Partie& Partie::donneInstance(){
	//	if (instanceUnique == nullptr) instanceUnique = new Partie;
	//	return *instanceUnique;
	//}
	//void Partie::libereInstance() {
	//	delete instanceUnique;
	//	instanceUnique = nullptr;
	//}
	//Partie::~Partie() {};
	//// end Singleton.

	void Partie::printInfoPioche() {
		std::cout << "\nPioche de Cartes Clan : " << piocheClan.getNbCartes() << " cartes restantes.\n";
	}
	void Partie::dealCartesClan() {
		if (piocheClan.getNbCartes() <= 0) {
			std::cout << "Il ne reste plus de cartes dans la pioche Clan.";
			return;
		}
		else {
			while (joueur1.getNbCartes() < nombreDeCartesChaqueJoueur) {
				joueur1.addCarte(piocheClan.piocher()); //checkpoint
			}
			while (joueur2.getNbCartes() < nombreDeCartesChaqueJoueur) {
				joueur2.addCarte(piocheClan.piocher()); //checkpoint
			}
		}
	}

	void Partie::getIndicesBornesJoueesUnCote(int indiceJoueur, std::vector<int>& bornesJoueesUnCote) {
		for (int indiceBorne = 0; indiceBorne < 9; indiceBorne++) {
			if (!(bornes[indiceBorne].estVide(indiceJoueur))) {
				bornesJoueesUnCote.push_back(indiceBorne);
			}
		}
	}
	void Partie::getIndicesBornesPasRevendiquees(std::vector<int>& bornesNonRevendiquees) {
		for (int indiceBorne = 0; indiceBorne < 9; indiceBorne++) {
			if (!(bornes[indiceBorne].isComplete())) {
				bornesNonRevendiquees.push_back(indiceBorne);
			}
		}
	}
	void Partie::getIndicesBornesPasRemplieUnCote(int indiceJoueur, std::vector<int>& getBornesPasRemplieUnCote) {
		for (int indiceBorne = 0; indiceBorne < 9; indiceBorne++) {
			if (!(bornes[indiceBorne].isCompleteUnJoueur(indiceJoueur))) {
				getBornesPasRemplieUnCote.push_back(indiceBorne);
			}
		}
	}
	int Partie::quiGagneCinqBornesDispersees() {
		// retourne 1 si joueur 1 a gagne, 2 si joueur 2 a gagne, et 0 si aucun joueur a gagne
		int compteurNbBornesGagneesJoueur1{ 0 };
		int compteurNbBornesGagneesJoueur2{ 0 };
		for (int i = 0; i < bornes.size(); i++) {
			if (bornes[i].getNumGagnant() == 1) compteurNbBornesGagneesJoueur1++;
			else if (bornes[i].getNumGagnant() == 2) compteurNbBornesGagneesJoueur2++;
		}
		if (compteurNbBornesGagneesJoueur1 >= 5) return 1; // joueur 1 a gagne
		else if (compteurNbBornesGagneesJoueur2 >= 5) return 2; // joueur 2 a gagne
		else return 0;
	}
	int Partie::quiGagneTroisBornesAdjacentes() {
		// retourne 1 si joueur 1 a gagne, 2 si joueur 2 a gagne, et 0 si aucun joueur a gagne
		for (int i = 0; i < bornes.size() - 2; i++) {
			if (bornes[i].getNumGagnant() == bornes[i + 1].getNumGagnant()\
				&& bornes[i + 1].getNumGagnant() == bornes[i + 2].getNumGagnant()) {
				if (bornes[i].getNumGagnant() == 1) return 1;
				else if (bornes[i].getNumGagnant() == 2) return 2;
			}
		}
		return 0;
	}

	void Partie::gererCarte(int& indiceCurrentBorne, int& indiceCarte, int indiceJoueur) {
		Joueur* joueurActuel = &joueur1;
		if (indiceJoueur == 2) joueurActuel = &joueur2;

		indiceCarte = joueurActuel->choisirIndiceCarte();
		// Si indiceCarte == -1, c'est une erreur (voir outils.h)
		// cette erreur peut se produire lorsqu'un joueur n'a plus de cartes par exemple
		if (indiceCarte == -1) return;

		const CarteClan* carte = dynamic_cast<const CarteClan*>(joueurActuel->getCarte(indiceCarte));

		std::cout << "\nVeuillez choisir une borne: ";
		std::vector<int> bornesPasRemplieUnCote;
		getIndicesBornesPasRemplieUnCote(indiceJoueur, bornesPasRemplieUnCote);
		if (bornesPasRemplieUnCote.empty()) return;
		indiceCurrentBorne = joueurActuel->choisirConstrained(bornesPasRemplieUnCote);

		bornes[indiceCurrentBorne].addCarteClan(joueurActuel->getNumJoueur(), carte); //checkpoint2
		joueurActuel->removeCarte(indiceCarte);
	}
	void Partie::lancer() {
		signal(SIGINT, signal_callback_handler); // handle ctrl + c
		std::cout << "Vous avez choisi la variante par defaut\n";
		suspendre();
		piocheClan.melanger();
		dealCartesClan();
		int indiceCurrentBorne{ 0 };
		int indiceCarte{ 0 };

		Joueur* joueurActuel = &joueur1;
		int indiceJoueurActuel{ 1 };
		while (true) {
			if (quiGagneTroisBornesAdjacentes() || quiGagneCinqBornesDispersees()) {
				break;
			}
			clear_screen();
			printAllBornes();
			printInfoPioche();
			std::cout << "\nC'EST LE TOUR DU JOUEUR " << indiceJoueurActuel << std::endl;
			joueurActuel->printCartes();
			gererCarte(indiceCurrentBorne, indiceCarte, indiceJoueurActuel);
			if (bornes[indiceCurrentBorne].isComplete() && bornes[indiceCurrentBorne].getNumGagnant() == 0) {
				// si borne complete, et qu'on a pas traite cette borne
				quandBorneRemplie(indiceCurrentBorne);
			}
			dealCartesClan();

			// changer le joueur
			if (indiceJoueurActuel == 1) joueurActuel = &joueur2;
			else if (indiceJoueurActuel == 2) joueurActuel = &joueur1;
			indiceJoueurActuel = (indiceJoueurActuel == 1) ? 2 : 1;

			// debug
			if (debug_mode == true) { // debug uniquement
				clear_screen();
				std::cout << "\n=================================================\n";
				std::cout << "\nDEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG\n";
				printAllBornes();
				printInfoPioche();
				std::cout << "\nC'EST LE TOUR DU JOUEUR " << indiceJoueurActuel << std::endl;
				std::cout << "\nCartes joueur 1:\n";
				joueur1.printCartes();
				std::cout << "\nCartes joueur 2:\n";
				joueur2.printCartes();
				StrategieJoueur* manual = new StrategieManuelle();
				setStrategieJoueur(1, manual);
				setStrategieJoueur(2, manual);
				std::cout << "\n=================================================\n";
				debug_mode = false;
				suspendre();
			}
		}
		printWinner();
	}
	void Partie::quandBorneRemplie(int indiceCurrentBorne) {
		Joueur* gagnantBorne = nullptr;
		int indiceJoueurGagnant = bornes[indiceCurrentBorne].quiGagne();
		if (indiceJoueurGagnant == 1)  gagnantBorne = &joueur1;
		else if (indiceJoueurGagnant == 2)  gagnantBorne = &joueur2;
		if (gagnantBorne != nullptr) {
			gagnantBorne->augScore();
		}
	}
	void Partie::printWinner() {
		clear_screen();
		printAllBornes();
		printInfoPioche();
		std::cout << "\n==============================================================\n";
		int troisBornesAdjacentes{ quiGagneTroisBornesAdjacentes()};
		int cinqBornesDispersees{ quiGagneCinqBornesDispersees() };
		int gagnantFinal = (troisBornesAdjacentes > cinqBornesDispersees) ? troisBornesAdjacentes : cinqBornesDispersees;
		if (gagnantFinal == 0) gagnantFinal = (joueur1.getScore() > joueur2.getScore()) ? 1 : 2;
		std::cout << "Nombre de bornes gagnees: " << std::endl;
		std::cout << "Joueur 1: " << joueur1.getScore() << std::endl;
		std::cout << "Joueur 2: " << joueur2.getScore() << std::endl;
		std::cout << "Le gagnant final est JOUEUR " << gagnantFinal << std::endl;
		if (troisBornesAdjacentes) std::cout << ", grace a 3 bornes adjacentes gagnees\n";
		if (cinqBornesDispersees) std::cout << ", grace a 5 bornes dispersees gagnees\n";
		suspendre();
	}
	

	void Partie::printAllBornes() {
		char separator = ' ';
		int largeur = 10;
		// Print "BORNE 1..."
		printLine("       ", largeur);
		for (int i = 0; i < 9; i++) {
			std::string str{ "BORNE " };
			str += std::to_string(i); // pour print "BORNE 1"
			printLine(str, largeur);
		}
		std::cout << std::endl;

		// Print chaque ligne de joueur 1
		for (int indiceCarte = 0; indiceCarte < 3; indiceCarte++) {
			std::string str{ "CARTE " };
			str += std::to_string(indiceCarte +1); // pour print "CARTE 1"
			printLine(str, largeur);
				for (int indiceBorne = 0; indiceBorne < 9; indiceBorne++) {
					std::cout << std::setw(largeur) << std::left << std::setfill(separator) << *bornes[indiceBorne].getCarte(1, indiceCarte);
				}
				std::cout << std::endl;
		}

		printLine("=======", largeur);
		// print "=======" pour separer les 2 joueurs, ou print le nom de joueur gagnant
		for (int i = 0; i < 9; i++) { 
			if (bornes[i].isComplete()) {
				if(bornes[i].getNumGagnant()==1)
					printLine("\033[48;5;14m\033[38;5;16mJOUER 1\033[0m   ", largeur);
				else if (bornes[i].getNumGagnant()==2)
					printLine("\033[48;5;14m\033[38;5;16mJOUER 2\033[0m   ", largeur);
			}
			else printLine("=======", largeur); 
		}
		std::cout << std::endl;

		// Print chaque ligne de joueur 2
		for (int indiceCarte = 0; indiceCarte < 3; indiceCarte++) {
			std::string str{ "CARTE " };
			str += std::to_string(indiceCarte + 1); 
			printLine(str, largeur);
			for (int indiceBorne = 0; indiceBorne < 9; indiceBorne++) {
				std::cout << std::setw(largeur) << std::left << std::setfill(separator) << *bornes[indiceBorne].getCarte(2, indiceCarte);
			}
			std::cout << std::endl;
		}
	}
}