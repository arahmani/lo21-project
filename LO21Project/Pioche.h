#pragma once
#include <vector>
#include <algorithm>
#include <random>
#include "Carte.h"

namespace SchottenTotten {
    template <typename TypeCarte> // class template
    class Pioche {
    protected:
        const TypeCarte** initialPioche; // liste des cartes � supprimer, car les cartes sont en composition avec les pioche et doivent �tre supprim�es
        std::vector<const TypeCarte*> pioche;
    public:
        void addToPioche(const TypeCarte* carte) { pioche.push_back(carte); }
        void melanger() {
            static std::random_device rd;
            static std::mt19937 rng(rd());
            std::shuffle(pioche.begin(), pioche.end(), rng);
        }
        const Carte* piocher() {
            if (!pioche.empty()) {
                const Carte* carte = pioche.back();
                pioche.pop_back();
                return carte;
            }
            else return nullptr;
        }
        int getNbCartes() { 
            int nbCartes = static_cast<int>(pioche.size());
            return nbCartes;
        }
        ~Pioche(){ delete[] initialPioche; }
    };

    class PiocheClan : public Pioche<CarteClan> {
    public:
        PiocheClan();
        ~PiocheClan();
    };

    class PiocheTactique : public Pioche<CarteTactique> {
    public:
        PiocheTactique();
        ~PiocheTactique();
    };
}