#include "Borne.h"
#include <iostream>
#include "Carte.h"

namespace SchottenTotten {
	int Borne::quiGagne() {
		int resultat = 0;
		if (ColinMaillard == false) {
			//en utilisant evaluer(), dterminer qui a gagn cette borne.
			TypeCombinaison combi1{ cartesClan_j1.evaluer() };
			TypeCombinaison combi2{ cartesClan_j2.evaluer() };
			if (combi1 == combi2) {
				// si cartes_joueur_1 et cartes_joueur_2 sont de meme type (suite, brelan, somme...)
				// alors on verifie leur sommes
				resultat = cartesClan_j1.sommeSuite() > cartesClan_j2.sommeSuite() ? 1 : 2;
				// si somme des puissances de cartes_joueur_1 > cartes_joueur_2 alors renvoie 1, sinon 2.
			}
			else
				resultat = (combi1 > combi2) ? 1 : 2;
			numGagnant = resultat;
			return resultat;
		}
		else if (ColinMaillard == true) {
			resultat = cartesClan_j2.sommeSuite() > cartesClan_j2.sommeSuite() ? 1 : 2;
			// si somme des puissances de cartes_joueur_1 > cartes_joueur_2 alors renvoie 1, sinon 2.
			numGagnant = resultat;
			return resultat;
		}
		
	}

	const CarteClan* Borne::getCarte(int indexJoueur, int indexCarte) {
		const CarteClan* carte = nullptr;
		if (indexJoueur == 1) carte = cartesClan_j1.getCarte(indexCarte);
		else if (indexJoueur == 2) carte = cartesClan_j2.getCarte(indexCarte);
		return carte;
	}
	void Borne::removeCarte(int indexJoueur, int indexCarte){
		if (indexJoueur == 1) cartesClan_j1.removeCarte(indexCarte);
		else if (indexJoueur == 2) cartesClan_j2.removeCarte(indexCarte);
	}
	void Borne::addCarteClan(int indexJoueur, const CarteClan* carte) {
		//check if reference is needed
		if (indexJoueur == 1) cartesClan_j1.addCarte(*carte);
		else if (indexJoueur == 2) cartesClan_j2.addCarte(*carte);
	}
	void Borne::addCarteTactique(int indexJoueur, const CarteTactique* carte) {
		//check if reference is needed
		if (indexJoueur == 1) cartesTactique_j1.push_back(carte);
		else if (indexJoueur == 2) cartesTactique_j2.push_back(carte);
	}
}