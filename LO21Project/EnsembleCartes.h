#pragma once
#include "Carte.h"
#include <vector>
//factory?
namespace SchottenTotten {
	enum TypeCombinaison {
		SOMME, SUITE, COULEUR, BRELAN, SUITECOULEUR
	};
	std::ostream& operator<< (std::ostream& strm, TypeCombinaison combi);

	class EnsembleCartes { 
		std::vector<const CarteClan*> cartes;
	public:
		int sommeSuite() const;
		bool memeCouleur() const;
		bool memePuissance() const;
		bool isSuite() const;
		void trierCartes();
		TypeCombinaison evaluer();

		void addCarte(const CarteClan& carte);
		const CarteClan* getCarte(int indexCarte);
		void removeCarte(int indexCarte) { cartes.erase(cartes.begin() + indexCarte); }

		int getNbCartes() const { 
			int nbCartes = static_cast<int>(cartes.size());
			return nbCartes;
		}
		void print() const;

		EnsembleCartes() = default;
	};
}
