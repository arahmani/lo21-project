#include "PartieTactique.h"
#include <iostream>
#include <iomanip>

namespace SchottenTotten {
	void PartieTactique::askCarteTactique(Joueur& joueurActuel) {
		// Demander si user veut tirer une carte de la pioche Tactique
		if (piocheTactique.getNbCartes() <= 0) {
			std::cout << "Il ne reste plus de cartes Tactique dans la pioche\n";
			return;
		}
		if (joueurActuel.getNbCartes() >= nombreDeCartesChaqueJoueur) {
			std::cout << "\nVous avez deja trop de cartes\n";
			return;
		}
		else {
			// Demander si user veut tirer une carte de la pioche Tactique
			int tactiqueOui{ joueurActuel.choisirSiTirerCarteTactique() };
			if (tactiqueOui == 1) {
				const Carte* carte = piocheTactique.piocher();
				joueurActuel.addCarte(carte);
			}
		}
	}
	void PartieTactique::retourCarte_a_Pioche(int indiceCarte, Joueur& joueurActuel) {
		const CarteTactique* carte = dynamic_cast<const CarteTactique*>(joueurActuel.getCarte(indiceCarte));
		if (carte != nullptr) {
			piocheTactique.addToPioche(carte);
		}
		// sinon carte Clan
		else {
			const CarteClan* carteC = dynamic_cast<const CarteClan*>(joueurActuel.getCarte(indiceCarte));
			piocheClan.addToPioche(carteC);
		}
		joueurActuel.removeCarte(indiceCarte);
		return;

	}
	void PartieTactique::actionJoker(int& indiceCurrentBorne, Joueur& joueurActuel) {
		int puiss{ joueurActuel.choisirPuissance()};
		int color{ joueurActuel.choisirCouleur()};

		std::cout << "\nVeuillez choisir une borne: ";
		std::vector<int> bornesPasRemplieUnCote;
		getIndicesBornesPasRemplieUnCote(joueurActuel.getNumJoueur(), bornesPasRemplieUnCote);
		if (bornesPasRemplieUnCote.empty()) return;
		indiceCurrentBorne = joueurActuel.choisirConstrained(bornesPasRemplieUnCote);

		const CarteClan* carte = new CarteClan(static_cast<Puissance>(puiss), static_cast<Couleur>(color));
		bornes[indiceCurrentBorne].addCarteClan(joueurActuel.getNumJoueur(), carte);
	}
	void PartieTactique::actionEspion(int& indiceCurrentBorne, Joueur& joueurActuel) {
		std::cout << "Espion: carte Clan de valeur 7 dont vous choisissez la couleur\n";
		int puiss{ 7 };
		int color{ joueurActuel.choisirCouleur() };
		
		std::cout << "\nVeuillez choisir une borne: ";
		std::vector<int> bornesPasRemplieUnCote;
		getIndicesBornesPasRemplieUnCote(joueurActuel.getNumJoueur(), bornesPasRemplieUnCote);
		if (bornesPasRemplieUnCote.empty()) return;
		indiceCurrentBorne = joueurActuel.choisirConstrained(bornesPasRemplieUnCote);

		const CarteClan* carte = new CarteClan(static_cast<Puissance>(puiss), static_cast<Couleur>(color));
		bornes[indiceCurrentBorne].addCarteClan(joueurActuel.getNumJoueur(), carte);
	}
	void PartieTactique::actionPorteBouclier(int& indiceCurrentBorne, Joueur& joueurActuel) {
		std::cout << "Porte Bouclier: carte Clan de valeur 1, 2 ou 3 dont vous choisissez la couleur\n";
		int puiss{ joueurActuel.choisir(1, 3, "Choisissez la puissance entre 1 et 3: ") };
		int color{ joueurActuel.choisirCouleur() };
		
		std::cout << "\nVeuillez choisir une borne: ";
		std::vector<int> bornesPasRemplieUnCote;
		getIndicesBornesPasRemplieUnCote(joueurActuel.getNumJoueur(), bornesPasRemplieUnCote);
		if (bornesPasRemplieUnCote.empty()) return;
		indiceCurrentBorne = joueurActuel.choisirConstrained(bornesPasRemplieUnCote);

		const CarteClan* carte = new CarteClan(static_cast<Puissance>(puiss), static_cast<Couleur>(color));
		bornes[indiceCurrentBorne].addCarteClan(joueurActuel.getNumJoueur(), carte);
	}
	void PartieTactique::actionChasseur(Joueur& joueurActuel) {
		std::cout << "Chasseur: piochez trois cartes. Renvoyez deux d'entre eux.\n";
		std::cout << "Choisissez: \n";
		std::cout << "1. Piochez 3 cartes Clan.\n";
		std::cout << "2. Piochez 2 cartes Clan et 1 cartes Tactique.\n";
		std::cout << "3. Piochez 1 carte Clan et 2 cartes Tactique.\n";
		std::cout << "4. Piochez 3 cartes Tactiques\n";
		int choix{ joueurActuel.choisir(1, 4, "Votre choix: ")};
		//Etape 1: piochez 3 cartes
		int nbCartesTirees{ 0 }; // car si joueur veut tirer 2 cartes tactique et qu'il en reste que 1
		const Carte* carte;
		switch (choix) {
		case 1:
			for (int i = 0; i < 3; i++) {
				carte = piocheClan.piocher();
				if (carte != nullptr) {
					joueurActuel.addCarte(carte);
					nbCartesTirees++;
				}
			}
			
			for (int i = 0; i < nbCartesTirees - 1; i++) {
				joueurActuel.printCartes();
				// renvoyez nb de cartes reellement tirees - 1
				int indice = joueurActuel.choisirIndiceCarte();
				retourCarte_a_Pioche(indice, joueurActuel);
			}
			break;
		case 2:
			for (int i = 0; i < 2; i++) {
				carte = piocheClan.piocher();
				if (carte != nullptr) {
					joueurActuel.addCarte(carte);
					nbCartesTirees++;
				}
			}
			carte = piocheTactique.piocher();
			if (carte != nullptr) {
				joueurActuel.addCarte(carte);
				nbCartesTirees++;
			}

			
			for (int i = 0; i < nbCartesTirees - 1; i++) {
				joueurActuel.printCartes();
				int indice = joueurActuel.choisirIndiceCarte();
				retourCarte_a_Pioche(indice, joueurActuel);
			}
			break;
		case 3:
			for (int i = 0; i < 2; i++) {
				carte = piocheTactique.piocher();
				if (carte != nullptr) {
					joueurActuel.addCarte(carte);
					nbCartesTirees++;
				}
			}
			carte = piocheClan.piocher();
			if (carte != nullptr) {
				joueurActuel.addCarte(carte);
				nbCartesTirees++;
			}
			
			for (int i = 0; i < nbCartesTirees - 1; i++) {
				joueurActuel.printCartes();
				int indice = joueurActuel.choisirIndiceCarte();
				retourCarte_a_Pioche(indice, joueurActuel);
			}
			break;
		case 4:
			for (int i = 0; i < 3; i++) {
				carte = piocheTactique.piocher();
				if (carte != nullptr) {
					joueurActuel.addCarte(carte);
					nbCartesTirees++;
				}
			}

			for (int i = 0; i < nbCartesTirees - 1; i++) {
				joueurActuel.printCartes();
				int indice = joueurActuel.choisirIndiceCarte();
				retourCarte_a_Pioche(indice, joueurActuel);
			}
			break;
		}
	}
	void PartieTactique::actionStratege(int& indiceCurrentBorne, Joueur& joueurActuel) {
		std::cout << "Stratege: choisissez une carte Clan de votre cote. Vous pouvez la deplacer / la defausser\n";
		int indiceJoueur{ joueurActuel.getNumJoueur() };
		int indiceBorneAuDebut{};

		std::cout << "Veuillez choisir une borne: ";
		std::vector<int> bornesJoueesParMoi;
		getIndicesBornesJoueesUnCote(joueurActuel.getNumJoueur(), bornesJoueesParMoi);
		indiceBorneAuDebut = joueurActuel.choisirConstrained(bornesJoueesParMoi);

		std::cout << "\n=================================================\n";
		std::cout << "||        Prenez une carte de VOTRE COTE       ||";
		std::cout << "\n=================================================\n" << std::endl;
		joueurActuel.printCartes();
		int indiceCarte{ joueurActuel.choisir(1, bornes[indiceBorneAuDebut].getNbCartesClan(indiceJoueur), "Choisissez une carte: ")};
		--indiceCarte; //car carte commence par 0
		const CarteClan* carte = bornes[indiceBorneAuDebut].getCarte(indiceJoueur, indiceCarte);
		std::cout << "Vous avez pris la carte " << *carte << std::endl;
		std::cout << "Vous voulez la deplacer ou? \n";
		std::cout << "1. Dans une autre borne\n";
		std::cout << "2. Dans la poubelle\n";
		int choix{ joueurActuel.choisir(1,2,"Votre choix: ")};
		if (choix == 1) {
			
			std::cout << "\nVeuillez choisir une borne: ";
			std::vector<int> bornesPasRemplieUnCote;
			getIndicesBornesPasRemplieUnCote(joueurActuel.getNumJoueur(), bornesPasRemplieUnCote);
			if (bornesPasRemplieUnCote.empty()) return;
			indiceCurrentBorne = joueurActuel.choisirConstrained(bornesPasRemplieUnCote);

			bornes[indiceCurrentBorne].addCarteClan(indiceJoueur, carte);
		}
		else if (choix == 2) defausse.ajouterCarte(carte);
		bornes[indiceBorneAuDebut].removeCarte(indiceJoueur, indiceCarte);
	}
	void PartieTactique::actionBanshee(Joueur& joueurActuel, Joueur& joueurAdverse) {
		std::cout << "Banshee: choisissez une carte Clan du cote adverse. Vous pouvez la defausser\n";
		int indiceJoueur{ joueurActuel.getNumJoueur() };
		int indiceAdverse = (indiceJoueur == 1) ? 2 : 1;
		int indiceBorne{};
		std::cout << "Veuillez choisir une borne: ";
		std::vector<int> bornesJoueesParAdverse;
		getIndicesBornesJoueesUnCote(indiceAdverse, bornesJoueesParAdverse);
		indiceBorne = joueurActuel.choisirConstrained(bornesJoueesParAdverse);
		std::cout << "\n=================================================\n";
		std::cout << "|| Prenez une carte du cote de VOTRE ADVERSAIRE ||";
		std::cout << "\n=================================================\n" << std::endl;
		joueurAdverse.printCartes();
		int indiceCarte{ joueurActuel.choisir(1, bornes[indiceBorne].getNbCartesClan(indiceAdverse), "Choisissez une carte: ")
	};
		--indiceCarte; //car carte commence par 0
		const CarteClan* carte = bornes[indiceBorne].getCarte(indiceAdverse, indiceCarte);
		
		std::cout << "Vous avez pris la carte " << *carte << std::endl;
		defausse.ajouterCarte(carte);
		bornes[indiceBorne].removeCarte(indiceAdverse, indiceCarte);
	}
	void PartieTactique::actionTraitre(int& indiceCurrentBorne, Joueur& joueurActuel, Joueur& joueurAdverse) {
		std::cout << "Traitre: choisissez une carte Clan du cote adverse. Vous pouvez lutiliser\n";
		int indiceJoueur{ joueurActuel.getNumJoueur() };
		int indiceAdverse = (indiceJoueur == 1) ? 2 : 1;
		int indiceBorneAuDebut{};
		std::cout << "Veuillez choisir une borne: ";
		std::vector<int> bornesJoueesParAdverse;
		getIndicesBornesJoueesUnCote(indiceAdverse, bornesJoueesParAdverse);
		indiceBorneAuDebut = joueurActuel.choisirConstrained(bornesJoueesParAdverse);
		std::cout << "\n=================================================\n";
		std::cout << "|| Prenez une carte du cote de VOTRE ADVERSAIRE ||";
		std::cout << "\n=================================================\n" << std::endl;
		joueurAdverse.printCartes();
		int indiceCarte{ joueurActuel.choisir(1, bornes[indiceBorneAuDebut].getNbCartesClan(indiceAdverse), "Choisissez une carte: ")};
		--indiceCarte; //car carte commence par 0
		const CarteClan* carte = bornes[indiceBorneAuDebut].getCarte(indiceAdverse, indiceCarte);
		std::cout << "Vous avez pris la carte " << *carte << std::endl;
		std::cout << "Maintenant vous pouvez lutiliser. Veuillez choisir une borne pour la placer\n";

		std::cout << "\nVeuillez choisir une borne: ";
		std::vector<int> bornesPasRemplieUnCote;
		getIndicesBornesPasRemplieUnCote(joueurActuel.getNumJoueur(), bornesPasRemplieUnCote);
		if (bornesPasRemplieUnCote.empty()) return;
		indiceCurrentBorne = joueurActuel.choisirConstrained(bornesPasRemplieUnCote);

		bornes[indiceCurrentBorne].addCarteClan(indiceJoueur, carte);
		bornes[indiceBorneAuDebut].removeCarte(indiceAdverse, indiceCarte);
	}
	void PartieTactique::gererCarte(int& indiceCurrentBorne, int& indiceCarte, int indiceJoueur) {
		Joueur* joueurActuel = &joueur1;
		Joueur* joueurAdverse = &joueur2;
		if (indiceJoueur == 2) {
			joueurActuel = &joueur2;
			joueurAdverse = &joueur1;
		}
		if (joueurActuel->getNbCartesTactiqueJouees() > joueurAdverse->getNbCartesTactiqueJouees()) {
			// Si le joueur a joue trop de cartes tactique
			std::cout << "\nVeuillez choisir une carte Clan\n";
			std::vector<int> getIndicesCartesClanEnMain;
			joueurActuel->getIndicesCartesClanEnMain(getIndicesCartesClanEnMain);
			if (getIndicesCartesClanEnMain.empty()) return;
			// S'il y a aucune carte Clan en main, le joueur peut alors rien faire. Il perd alors son tours.
			indiceCarte = joueurActuel->choisirConstrained(getIndicesCartesClanEnMain);
		}
		else indiceCarte = joueurActuel->choisirIndiceCarte();
		// Si indiceCarte == -1, c'est une erreur (voir outils.h)
		// cette erreur peut se produire lorsqu'un joueur n'a plus de cartes par exemple
		if (indiceCarte == -1) return;

		const Carte* carte = joueurActuel->getCarte(indiceCarte);
		if(carte->quelTypeCarte() == TACTIQUE) {
			gererCarteTactique(indiceCurrentBorne, indiceCarte, indiceJoueur);
			return;
		}
		else {
			const CarteClan* carteClan = dynamic_cast<const CarteClan*>(carte);
			std::cout << "\nVeuillez choisir une borne: ";
			std::vector<int> bornesPasRemplieUnCote;
			getIndicesBornesPasRemplieUnCote(indiceJoueur, bornesPasRemplieUnCote);
			if (bornesPasRemplieUnCote.empty()) return;
			indiceCurrentBorne = joueurActuel->choisirConstrained(bornesPasRemplieUnCote);
			bornes[indiceCurrentBorne].addCarteClan(joueurActuel->getNumJoueur(), carteClan);
			joueurActuel->removeCarte(indiceCarte);
		}
	}
	void PartieTactique::gererCarteTactique(int& indiceCurrentBorne, int& indiceCarte, int indiceJoueur) {
		Joueur* joueurActuel = &joueur1;
		Joueur* joueurAdverse = &joueur2;
		if (indiceJoueur == 2) {
			joueurActuel = &joueur2;
			joueurAdverse = &joueur1;
		}

		std::cout << "\n~~~~~~~~~~~~~~~~~~~~~CARTE TACTIQUE~~~~~~~~~~~~~~~~~~~~\n";
		const CarteTactique* carte = dynamic_cast<const CarteTactique*>(joueurActuel->getCarte(indiceCarte));
		
		int numTactique = carte->getTypeCarteTactique();
		joueurActuel->augNbCartesTactiqueJouees();
		joueurActuel->removeCarte(indiceCarte);
		switch (numTactique) {
		case JOKER1:
			std::cout << "Joker 1: carte Clan dont vous choisissez la couleur et la valeur\n";
			actionJoker(indiceCurrentBorne , *joueurActuel);
			break;
		case JOKER2:
			std::cout << "Joker 2: carte Clan dont vous choisissez la couleur et la valeur\n";
			actionJoker(indiceCurrentBorne, *joueurActuel);
			break;
		case ESPION:
			actionEspion(indiceCurrentBorne, *joueurActuel);
			break;
		case PORTEBOUCLIER:
			actionPorteBouclier(indiceCurrentBorne, *joueurActuel);
			break;
		case COLINMAILLLARD:
			std::cout << "Colin Maillard: C'est la valeur des cartes jouees sur cette borne qui compte,\n";
			std::cout << "pas des combinaisons\n";
			indiceCurrentBorne = joueurActuel->choisirIndiceBorne();
			bornes[indiceCurrentBorne].activerColinMaillard();
			break;
		case COMBATDEBOUE:
			std::cout << "Combat De Boue: vous devez desormais poser quatre cartes\n";
			indiceCurrentBorne = joueurActuel->choisirIndiceBorne();
			bornes[indiceCurrentBorne].activerCombatDeBoue();
			break;
		case CHASSEUR:
			actionChasseur(*joueurActuel);
			defausse.ajouterCarte(carte);
			break;
		case STRATEGE:
			actionStratege(indiceCurrentBorne, *joueurActuel);
			defausse.ajouterCarte(carte);
			break;
		case BANSHEE:
			actionBanshee(*joueurActuel, *joueurAdverse);
			defausse.ajouterCarte(carte);
			break;
		case TRAITRE:
			actionTraitre(indiceCurrentBorne, *joueurActuel, *joueurAdverse);
			defausse.ajouterCarte(carte);
			break;
		}
		// Ajoute la gestion de deplacement Colin Maillard & combat de boue.
		std::cout << "\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";
		return;
	}
	void PartieTactique::lancer() {
		signal(SIGINT, signal_callback_handler); // handle ctrl + c
		std::cout << "Vous avez choisi la variante Tactique.\n";
		nombreDeCartesChaqueJoueur = 7;
		suspendre();

		piocheClan.melanger();
		piocheTactique.melanger();

		dealCartesClan();
		int indiceCurrentBorne{ 0 };
		int indiceCarte{ 0 };
		Joueur* joueurActuel = &joueur1;
		int indiceJoueurActuel{ 1 };
		while (true) {
			if (quiGagneTroisBornesAdjacentes() || quiGagneCinqBornesDispersees() ) {
				break;
			}
			clear_screen();
			printAllBornes();
			printInfoPioche();
			std::cout << "\nC'EST LE TOUR DU JOUEUR " << indiceJoueurActuel << std::endl;
			joueurActuel->printCartes();
			gererCarte(indiceCurrentBorne, indiceCarte, indiceJoueurActuel);
			for (int i = 0; i < 9; i++) {
				if (bornes[i].isComplete() && bornes[i].getNumGagnant() == 0) {
					// si borne complete, et qu'on a pas traite cette borne
					quandBorneRemplie(i);
				}
			}
			askCarteTactique(*joueurActuel);
			dealCartesClan();
			
			/// changer le joueur
			if (indiceJoueurActuel == 1) joueurActuel = &joueur2;
			else if (indiceJoueurActuel == 2) joueurActuel = &joueur1;
			indiceJoueurActuel = (indiceJoueurActuel == 1) ? 2 : 1;

			// debug
			if (debug_mode == true) { // debug uniquement
				clear_screen();
				std::cout << "\n=================================================\n";
				std::cout << "\nDEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG\n";
				printAllBornes();
				printInfoPioche();
				std::cout << "\nC'EST LE TOUR DU JOUEUR " << indiceJoueurActuel << std::endl;
				std::cout << "\nCartes joueur 1:\n";
				joueur1.printCartes();
				std::cout << "\nCartes joueur 2:\n";
				joueur2.printCartes();
				StrategieJoueur* manual = new StrategieManuelle();
				setStrategieJoueur(1, manual);
				setStrategieJoueur(2, manual);
				std::cout << "\n=================================================\n";
				debug_mode = false;
				suspendre();
			}
		}
		printWinner();
	}
	void PartieTactique::printInfoPioche() {		
		std::cout << "\nPioche de Cartes Clan : " << piocheClan.getNbCartes() << " cartes restantes.\n";
		std::cout << "Pioche de Cartes Tactique : " << piocheTactique.getNbCartes() << " cartes restantes.\n";
		std::cout << "Nombre de Cartes Tactique que joueur 1 a jouees: " << joueur1.getNbCartesTactiqueJouees() << std::endl;
		std::cout << "Nombre de Cartes Tactique que joueur 2 a jouees: " << joueur2.getNbCartesTactiqueJouees() << std::endl;
		defausse.printCartes();
		// to do: debug
	}
	void PartieTactique::printAllBornes() {
		char separator = ' ';
		int largeur = 10;
		// Print "BORNE 1..."
		printLine("       ", largeur);
		for (int i = 0; i < 9; i++) {
			std::string str{ "BORNE " };
			str += std::to_string(i); // pour print "BORNE 1"
			printLine(str, largeur);
		}
		std::cout << std::endl;

		// Print chaque ligne de joueur 1
		for (int indiceCarte = 0; indiceCarte < 4; indiceCarte++) {
			std::string str{ "CARTE " };
			str += std::to_string(indiceCarte + 1); // pour print "CARTE 1"
			printLine(str, largeur);
			for (int indiceBorne = 0; indiceBorne < 9; indiceBorne++) { 
				if (indiceCarte == 3 && bornes[indiceBorne].getNbMaxiCartes() != 4) {
					// si c'est le 4eme carte dans la borne indiceCarte == 3
					// si Combat de Boue nest pas active, alors print rien. Sinon print la carte.
					std::cout << std::setw(largeur) << std::left << std::setfill(separator) << "     ";
				}
				else {
					const CarteClan& carte = *bornes[indiceBorne].getCarte(1, indiceCarte);
					std::cout << std::setw(largeur) << std::left << std::setfill(separator) << carte;
				}
			}
					
			std::cout << std::endl;
		}

		
		printLine("=======", largeur);
		// print "=======" pour separer les 2 joueurs, ou print le nom de joueur gagnant
		for (int i = 0; i < 9; i++) { 
			if (bornes[i].isComplete()) {
				if (bornes[i].getNumGagnant() == 1)
					printLine("\033[48;5;14m\033[38;5;16mJOUER 1\033[0m   ", largeur);
				else if (bornes[i].getNumGagnant() == 2)
					printLine("\033[48;5;14m\033[38;5;16mJOUER 2\033[0m   ", largeur);
				else printLine("=READY=", largeur);
			}
			else if (bornes[i].statusColinMaillard()) printLine("\033[48;5;202m\033[38;5;231m=COLIN=\033[0m   ", largeur);
			//print "=COLIN="
			else printLine("=======", largeur); 
		}
		std::cout << std::endl;

		// Print chaque ligne de joueur 2
		for (int indiceCarte = 0; indiceCarte < 4; indiceCarte++) {
			std::string str{ "CARTE " };
			str += std::to_string(indiceCarte + 1); // pour print "CARTE 1"
			printLine(str, largeur);
			for (int indiceBorne = 0; indiceBorne < 9; indiceBorne++) {
				if (indiceCarte == 3 && bornes[indiceBorne].getNbMaxiCartes() != 4) {
					// si c'est le 4eme carte dans la borne indiceCarte == 3
					// si Combat de Boue nest pas active, alors print rien. Sinon print la carte.
					std::cout << std::setw(largeur) << std::left << std::setfill(separator) << "     ";
				}
				else {
					const CarteClan& carte = *bornes[indiceBorne].getCarte(2, indiceCarte);
					std::cout << std::setw(largeur) << std::left << std::setfill(separator) << carte;
				}
			}
			std::cout << std::endl;
		}
	}
}