#include <iostream>
#include <iomanip>
#include "PartieExperts.h"
namespace SchottenTotten {
	void PartieExperts::lancer() {
		signal(SIGINT, signal_callback_handler); // handle ctrl + c
		std::cout << "Vous avez choisi la variante Experts. Vous ne pouvez revendiquer une Borne qu'au debut de votre tour.\n";
		nombreDeCartesChaqueJoueur = 7;
		suspendre();
		if(debug_mode)
		piocheClan.melanger();
		piocheTactique.melanger();

		dealCartesClan();
		int indiceCurrentBorne{ 0 };
		int indiceCarte{ 0 };
		Joueur* joueurActuel = &joueur1;
		int indiceJoueurActuel{ 1 };
		std::vector<int> listeBornesEvaluer;
		int nb_manches{ 0 }; // pour debug uniquement
		while (true) {
			if (quiGagneTroisBornesAdjacentes() || quiGagneCinqBornesDispersees()) {
				break;
			}
			clear_screen();
			printAllBornes();
			printInfoPioche();
			std::cout << "\nC'EST LE TOUR DU JOUEUR " << indiceJoueurActuel << std::endl;
			joueurActuel->printCartes();
			gererCarte(indiceCurrentBorne, indiceCarte, indiceJoueurActuel);
			// apres avoir jouee une carte, il faut attendre le debut du prochain tour
			// car cest la variante experts
			if (!listeBornesEvaluer.empty()){
				int ancienIndiceBorne = listeBornesEvaluer.back();
				quandBorneRemplie(ancienIndiceBorne);
				listeBornesEvaluer.pop_back();
			}

			for (int i = 0; i < 9; i++) {
				if (bornes[i].isComplete() && bornes[i].getNumGagnant() == 0) {
					// si borne complete, et qu'on a pas traite cette borne
					listeBornesEvaluer.push_back(i);
				}
			}

			// debug
			if (debug_mode == true) { // debug uniquement
				clear_screen();
				std::cout << "\n=================================================\n";
				std::cout << "\nDEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG\n";
				printAllBornes();
				printInfoPioche();
				std::cout << "\nC'EST LE TOUR DU JOUEUR " << indiceJoueurActuel << std::endl;
				std::cout << "\nCartes joueur 1:\n";
				joueur1.printCartes();
				std::cout << "\nCartes joueur 2:\n";
				joueur2.printCartes();
				StrategieJoueur* manual = new StrategieManuelle();
				setStrategieJoueur(1, manual);
				setStrategieJoueur(2, manual);
				std::cout << "\n=================================================\n";
				debug_mode = false;
				suspendre();
			}

			askCarteTactique(*joueurActuel);
			dealCartesClan();

			/// changer le joueur
			if (indiceJoueurActuel == 1) joueurActuel = &joueur2;
			else if (indiceJoueurActuel == 2) joueurActuel = &joueur1;
			indiceJoueurActuel = (indiceJoueurActuel == 1) ? 2 : 1;
			nb_manches++;
		}
		printWinner();
	}
}