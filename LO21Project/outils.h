#pragma once
#include <iostream>
#include <string>
#include <iomanip>
#include <vector>
#include <random>
#include <limits>
#include <signal.h>

#include <thread>
#include <chrono>

#ifdef _WIN32
    constexpr auto OPERATING_SYSTEM = "Windows";
#else // assuming POSIX
    constexpr auto OPERATING_SYSTEM = "POSIX";
#endif

constexpr int TIME_AI_SLEEPS_MILLISECONDS = 0; 
// pour changer le nombre de milliseconds que l'IA attendra apr�s avoir fait un choix.


extern bool debug_mode;

namespace SchottenTotten {
    static void clear_screen() {
        //pour nettoyer la console
        if(std::string(OPERATING_SYSTEM) == "Windows")
                std::system("cls");
        else //POSIX
                std::system("clear");
    }
    static void suspendre() {
        //pour "appuyer sur n'importe quel bouton pour continuer"
        if (std::string(OPERATING_SYSTEM) == "Windows")
                std::system("pause");
        else //POSIX
                std::system("read");
    }

    template<typename T> T askUserInput(T debut, T fin, const std::string& message) {
        T input{};
        if (fin < debut) return -1;
        // cette erreur peut se produire lorsqu'un joueur n'a plus de cartes par exemple
        while (true) {
            std::cout << message;
            if (std::cin >> input) {
                // type input correct
                if (debut <= input && input <= fin) {
                    // input dans la plage attendue
                    break;  // Exit the loop
                }
                else {
                    std::cout << "Choix invalide. Valeur hors de la plage attendue. " << std::endl;
                }
            }
            else {
                std::cout << "Choix invalide, type incorrect. " << std::endl;
                std::cin.clear();  // Clear the error flag
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');  // Discard invalid input
            }
        }
        return input;
    }
    template<typename T>T askUserInputConstrained(const std::vector<T>& options) {
        while (true) {
            std::cout << "Veuillez choisir entre ces options: ";
            for (const T& option : options) {
                std::cout << option << " ";
            }
            std::cout << std::endl;

            T choix{};
            if (std::cin >> choix) {
                // Check if the choice is within the list of options
                auto it = std::find(options.begin(), options.end(), choix);
                if (it != options.end()) {
                    return choix;
                }
            }
            std::cout << "Choix invalide. Reessayez" << std::endl;
            // Clear the input buffer
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
    }

    template<typename T>T randomInput(T debut, T fin) {
        if (fin < debut) return -1;
        // cette erreur peut se produire lorsqu'un joueur n'a plus de cartes par exemple
        std::random_device rd;
        std::mt19937 rng(rd());
        std::uniform_int_distribution<T> distribution(debut, fin);
        std::cout << "\nChoix: " << distribution(rng) << std::endl;
        std::this_thread::sleep_for(std::chrono::milliseconds(TIME_AI_SLEEPS_MILLISECONDS));
        //suspendre();
        return distribution(rng);
    }
    template<typename T>T randomInputConstrained(const std::vector<T>& options) {
        std::random_device rd;
        std::mt19937 generator(rd());
        std::uniform_int_distribution<T> distribution(0, options.size() - 1);
        T index = distribution(generator);
        std::cout << "\nChoix entre options: " << options[index] << std::endl;
        std::this_thread::sleep_for(std::chrono::milliseconds(TIME_AI_SLEEPS_MILLISECONDS));
        //suspendre();
        return options[index];
    }

    template<typename T> void printLine(T t, const int& largeur)
    {
        char separator = ' ';
        std::cout << std::left << std::setw(largeur) << std::setfill(separator) << t;
    }
    
    static void signal_callback_handler(int signum) {
        debug_mode = true;
        return;
    }

}