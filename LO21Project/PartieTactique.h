#pragma once
#include "Partie.h"

namespace SchottenTotten {
	class PartieTactique : public Partie {
	protected:
		PiocheTactique piocheTactique;
		Defausse defausse;
	public:
		void askCarteTactique(Joueur& joueurActuel); //demander si le joueur veut tirer une carte tactique ou pas
		void retourCarte_a_Pioche(int indiceCarte, Joueur& joueurActuel); //retourner une carte a la pioche correspondante
		void actionJoker(int& indiceCurrentBorne, Joueur& joueurActuel);
		void actionEspion(int& indiceCurrentBorne, Joueur& joueurActuel);
		void actionPorteBouclier(int& indiceCurrentBorne, Joueur& joueurActuel);
		void actionChasseur(Joueur& joueurActuel);
		void actionStratege(int& indiceCurrentBorne, Joueur& joueurActuel);
		void actionBanshee(Joueur& joueurActuel, Joueur& joueurAdverse);
		void actionTraitre(int& indiceCurrentBorne, Joueur& joueurActuel, Joueur& joueurAdverse);
		void gererCarte(int& indiceCurrentBorne, int& indiceCarte, int indiceJoueur) override; //voir si carte jouable sur une borne et la deposer
		//int pass par reference parce qu'on permet joueur a rejouer
		void gererCarteTactique(int& indiceCurrentBorne, int& indiceCarte, int indiceJoueur); 
		//int pass par reference parce qu'on permet joueur a rejouer
		void printInfoPioche() override;
		void lancer() override;
		void printAllBornes() override;
	};
}