#include "EnsembleCartes.h"
#include <iostream>

namespace SchottenTotten {
	std::ostream& operator<< (std::ostream& strm, TypeCombinaison combi) {
		const std::string nameCombi[] = { "SOMME", "SUITE", "COULEUR", "BRELAN", "SUITECOULEUR" };
		return strm << nameCombi[combi];
	}

	int EnsembleCartes::sommeSuite() const {
		//Calculer la somme de puissance d'une suite de 3 ou 4 cartes 
		int somme{ 0 };
		for (int i = 0; i < cartes.size(); i++) {
			somme += cartes[i]->getPuissance();
		}
		return somme;
	}

	bool EnsembleCartes::memeCouleur() const {
		//vrifier si 3 ou 4 cartes sont de mme couleur
		if (cartes.empty()) return false;
		for (int i = 0; i < cartes.size() - 1; i++) {
			if (cartes[i]->getCouleur() != cartes[i + 1]->getCouleur()) return false;
		}
		return true;
	}

	bool EnsembleCartes::memePuissance() const {
		//vrifier si 3 ou 4 cartes sont de mme puissance
		if (cartes.empty()) return false;
		for (int i = 0; i < cartes.size() - 1; i++) {
			if (cartes[i]->getPuissance() != cartes[i + 1]->getPuissance()) return false;
		}
		return true;
	}

	bool EnsembleCartes::isSuite() const {
		//vrifier si 3 ou 4 cartes constituent une suite
		if (cartes.empty()) return false;
		for (int i = 0; i < cartes.size() - 1; i++) {
			if (cartes[i]->getPuissance() + 1 != cartes[i + 1]->getPuissance()) return false;
		}
		return true;
	}

	void EnsembleCartes::print() const 
	{
		for (int i = 0; i < cartes.size(); i++) {
			std::cout << i + 1 << ". ";
			cartes[i]->print();
		}
	}

	void EnsembleCartes::addCarte(const CarteClan& carte) {
		if (cartes.size() <= 4) cartes.push_back(&carte);
		else perror("Erreur fatale: combinaison > 4.");
	}

	const CarteClan* EnsembleCartes::getCarte(int indexCarte) {
		if (indexCarte >= cartes.size()) return nullptr; // attention >=
		else return cartes[indexCarte];
	}


	void EnsembleCartes::trierCartes() {
		//Trier 3 ou 4 cartes et retourner le type (brelan, suite...)
		if (cartes.size() == 3) {
			if (cartes[0]->getPuissance() > cartes[2]->getPuissance()) std::swap(cartes[0], cartes[2]);
			if (cartes[0]->getPuissance() > cartes[1]->getPuissance()) std::swap(cartes[0], cartes[1]);
			if (cartes[1]->getPuissance() > cartes[2]->getPuissance()) std::swap(cartes[1], cartes[2]);
		}
		else if (cartes.size() == 4) {
			if (cartes[0]->getPuissance() > cartes[1]->getPuissance()) std::swap(cartes[0], cartes[1]);
			if (cartes[2]->getPuissance() > cartes[3]->getPuissance()) std::swap(cartes[2], cartes[3]);
			if (cartes[0]->getPuissance() > cartes[2]->getPuissance()) std::swap(cartes[0], cartes[2]);
			if (cartes[1]->getPuissance() > cartes[3]->getPuissance()) std::swap(cartes[1], cartes[3]);
			if (cartes[1]->getPuissance() > cartes[2]->getPuissance()) std::swap(cartes[1], cartes[2]);
		}
		else perror("Erreur fatale, nb cartes pas == 3 ou 4");
	}

	TypeCombinaison EnsembleCartes::evaluer() {
		//Trier 3 ou 4 cartes et retourner le type (brelan, suite...)
		if (cartes.size() != 3 && cartes.size() != 4) perror("Erreur Fatale. Pas une borne?");

		//Trier les cartes selon ses puissances
		this->trierCartes();

		if (this->memePuissance()) return BRELAN;
		else if (this->memeCouleur()) {
			if (this->isSuite()) return SUITECOULEUR;
			else return COULEUR;
		}
		else if (this->isSuite()) return SUITE;
		else return SOMME;

	}
}