﻿#include "outils.h"
#include "Partie.h"
#include "PartieTactique.h"
#include "PartieExperts.h"

using namespace SchottenTotten;

bool debug_mode = false;
int main()
{
    Partie* partie = nullptr;
    std::cout << "Bienvenue au Schotten-Totten!\n";
    std::cout << "Choisissez la variante que vous voulez: \n";
    std::cout << "1. Variante classique.\n";
    std::cout << "2. Variante Tactique avec des cartes Tactique.\n";
    std::cout << "3. Variante Experts, cartes Tactique, vous ne pouvez revendiquer une Borne qu'au debut de votre tour";
    std::cout << ", avant de jouer une carte. \n";
    std::cout << "Attention: si vous voulez, une IA peut jouer contre une autre IA! \n";
    int choixVariante{ askUserInput<int>(1, 3, "Votre choix: ") };
    int stratJoueur1{ 0 };
    int stratJoueur2{ 0 };
    stratJoueur1 = askUserInput<int>(0, 1,"Joueur 1 est un vrai joueur ou une IA? 0 pour Joueur, 1 pour IA: ");
    stratJoueur2 = askUserInput<int>(0, 1,"Joueur 2 est un vrai joueur ou une IA? 0 pour Joueur, 1 pour IA: ");
    StrategieJoueur* machine = new StrategieRandom();
    // Design pattern Template Method
    switch (choixVariante) {
    case 1:
        partie = new Partie();
        break;
    case 2:
        partie = new PartieTactique();
        break;
    case 3:
        partie = new PartieExperts();
        break;
    }
    if (stratJoueur1 == 1) partie->setStrategieJoueur(1, machine);
    if (stratJoueur2 == 1) partie->setStrategieJoueur(2, machine);
    partie->lancer();
    return 0;
}

