#include "Joueur.h"
#include "Partie.h"
#include <iostream>

namespace SchottenTotten {
    void Joueur::printCartes() {
        std::cout << "\nCartes: \n";
        int i{ 0 };
        for (const Carte* carte : cartesEnMain) {
            std::cout << i << ".  ";
            carte->print();
            ++i;
        }
    }
    void Joueur::addCarte(const Carte* carte) {
        if (carte == nullptr) {
            std::cout << "\nIl en reste plus de carte\n";
            return;
        }
        cartesEnMain.push_back(carte);
        nbCartesEnMain[carte->quelTypeCarte()]++;
        return;
    }
    void Joueur::getIndicesCartesClanEnMain(std::vector<int>& getIndicesCartesClanEnMain) {
        int i{ 0 };
        for (const Carte* carte : cartesEnMain) {
            const CarteClan* test = dynamic_cast<const CarteClan*>(carte);
            if (test != nullptr) {
                getIndicesCartesClanEnMain.push_back(i);
            }
            i++;
        }
    }
    void Joueur::removeCarte(int index) {
        int typeCarte = cartesEnMain[index]->quelTypeCarte();
        cartesEnMain.erase(cartesEnMain.begin() + index); 
        nbCartesEnMain[typeCarte]--;
    }
    void Joueur::removeCarte(Carte* carte) {
        cartesEnMain.erase(std::remove(cartesEnMain.begin(), cartesEnMain.end(), carte), cartesEnMain.end());
        nbCartesEnMain[carte->quelTypeCarte()]--;
    }
    const Carte* Joueur::getCarte(int index) {
        try {
            return cartesEnMain.at(index);
        }
        catch (const std::out_of_range& e) {
            std::cout << "Error: " << e.what() << std::endl;
            return nullptr;
        }
    }
}