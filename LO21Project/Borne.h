#pragma once
#include "EnsembleCartes.h"
#include "Carte.h"
//factory?

namespace SchottenTotten {
	class Borne {
		EnsembleCartes cartesClan_j1;
		EnsembleCartes cartesClan_j2;
		std::vector<const CarteTactique*> cartesTactique_j1;
		std::vector<const CarteTactique*> cartesTactique_j2;
		int numGagnant{ 0 }; // "1" ou "2" pour joueur 1 ou 2.
		int nbMaxiCartes{ 3 };// nombre de cartes quil faut poser sur cette borne

		bool ColinMaillard{ false }; //additionnez uniquement la valeur des cartes jouees sur cette borne,
							//sans tenir compte des eventuelles combinaisons
	public:
		bool isComplete() const {
			//vrifier si cette borne est revendique
			return (cartesClan_j1.getNbCartes() == nbMaxiCartes && nbMaxiCartes == cartesClan_j2.getNbCartes());
		}
		
		bool isCompleteUnJoueur(int indiceJoueur) const {
			//vrifier si cette borne est remplie dun cote, en specifiant un numJoueur
			if (indiceJoueur == 1) return (cartesClan_j1.getNbCartes() == nbMaxiCartes);
			else if (indiceJoueur == 2) return (cartesClan_j2.getNbCartes() == nbMaxiCartes);
		}

		bool isCompleteUnJoueur() const {
			//vrifier si cette borne est remplie dun cote
			return (cartesClan_j1.getNbCartes() == nbMaxiCartes || nbMaxiCartes == cartesClan_j2.getNbCartes());
		}

		bool estVide(int indiceJoueur) const {
			if (indiceJoueur == 1) return (cartesClan_j1.getNbCartes() <= 0);
			else if (indiceJoueur == 2) return (cartesClan_j2.getNbCartes() <= 0);
		}

		void setNumGagnant(int num) { numGagnant = num; } // "1" ou "2" pour joueur 1 ou 2.
		int getNumGagnant() const { return numGagnant; }
		int getNbMaxiCartes() const { return nbMaxiCartes; }
		int getNbCartesClan1() const { return cartesClan_j1.getNbCartes(); }
		int getNbCartesClan2() const { return cartesClan_j2.getNbCartes(); }

		int getNbCartesClan(int indiceJoueur) const {
			if (indiceJoueur == 1) return cartesClan_j1.getNbCartes();
			else if (indiceJoueur == 2) return cartesClan_j2.getNbCartes();
		}

		int nbCartesAJouerJ1() const { return nbMaxiCartes - cartesClan_j1.getNbCartes(); }
		int nbCartesAJouerJ2() const { return nbMaxiCartes - cartesClan_j2.getNbCartes(); }
		int nbCartesAJouer(int indiceJoueur) const {
			if (indiceJoueur == 1) return nbMaxiCartes - cartesClan_j1.getNbCartes();
			else if (indiceJoueur == 2) return nbMaxiCartes - cartesClan_j2.getNbCartes();
		}
		const CarteClan* getCarte(int indexJoueur, int indexCarte);
		void removeCarte(int indexJoueur, int indexCarte);
		void addCarteClan(int indexJoueur, const CarteClan* carte);
		void addCarteTactique(int indexJoueur, const CarteTactique* carte);
		bool statusColinMaillard() const { return ColinMaillard; }
		void activerColinMaillard() { ColinMaillard = true; }
		void desactiverColinMaillard() { ColinMaillard = false; }
		void activerCombatDeBoue() { nbMaxiCartes = 4; }
		void desactiverCombatDeBoue() { nbMaxiCartes = 3; }

		int quiGagne();


		Borne() = default;
	};

}